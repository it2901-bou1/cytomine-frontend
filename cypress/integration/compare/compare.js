// /* eslint-disable */

it('Test login', () => {
  cy.visit('/');
  //   Enter username and password
  cy.get(':nth-child(1) > .control > .input').type('admin');
  cy.get(':nth-child(2) > .control > .input').type(
    '411a2d7c-2b24-470f-bb61-1794ff658ed9{enter}'
  );
  cy.contains('Welcome to the Cytomine software');
});

it('Opening compare via compare link', () => {
  cy.visit('#/compare-list');
  cy.get(':nth-child(3) > :nth-child(8) > span > .button').click();
  cy.get(':nth-child(1) > .buttonContainer > .submitCompare');
});

it('Opening compare via project link', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.get(':nth-child(4) > a').click();
  cy.get(':nth-child(1) > .buttonContainer > .submitCompare');
});

it('Comparing via project link -  Picture #1', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.get(':nth-child(4) > a').click();
  cy.get(':nth-child(1) > .buttonContainer > .submitCompare').click();
  cy.get('.notification-content').contains('Ranking');
});

it('Comparing via project link -  Picture #2', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.get(':nth-child(4) > a').click();
  cy.get(':nth-child(2) > .buttonContainer > .submitCompare').click();
  cy.get('.notification-content').contains('Ranking');
});

it('Comparing - check post data sent to API', () => {
  cy.intercept({
    method: 'POST',
    url: '/props/*'
  }).as('postCheck');

  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.get(':nth-child(4) > a').click();
  cy.get(':nth-child(1) > .buttonContainer > .submitCompare').click();
  cy.wait('@postCheck').then(interception => {
    assert.isNotNull(interception.response.body, 'API post call has data');
  });
});

it('Comparing - check get data received from API', () => {
  cy.intercept({
    method: 'GET',
    url: '/api/*'
  }).as('getCheck');
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.get(':nth-child(4) > a').click();
  cy.wait('@getCheck').then(interception => {
    assert.isNotNull(interception.response.body, 'API response has data');
  });
});

it('Test logout', () => {
  cy.visit('/');
  cy.get('.navbar-burger').click();
  cy.contains('Logout').click();
});

it('Test without login', () => {
  cy.visit('#/project/119698/compare');
  // Check that we are going to login page
  cy.get(':nth-child(1) > .control > .input');
});
