/* eslint-disable */
it('Test login', () => {
  cy.visit('/');
  //   Enter username and password
  cy.get(':nth-child(1) > .control > .input').type('admin');
  cy.get(':nth-child(2) > .control > .input').type(
    '411a2d7c-2b24-470f-bb61-1794ff658ed9{enter}'
  );
  cy.contains('Welcome to the Cytomine software');
});

it('Open Compare Absolute', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
});

it('Check that submit button is disabled #1 ', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get('.is-link').should('be.disabled');
});

it('Check that submit button is disabled #2', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(1)').click();
  cy.get('.is-link').should('be.disabled');
});

it('Check that submit button is enabled ', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(1)').click();
  cy.get('.is-link').should('not.be.disabled');
});

// Test different choices
it('Comparing Absolute #1', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(1)').click();
  cy.get('.is-link').click();
  cy.get('.notification-content').contains('Ranking');
});

it('Comparing Absolute #2', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(2)').click();
  cy.get('.is-link').click();
  cy.get('.notification-content').contains('Ranking');
});

it('Comparing Absolute #3', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(3)').click();
  cy.get('.is-link').click();
  cy.get('.notification-content').contains('Ranking');
});

it('Comparing Absolute #4', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(3)').click();
  cy.get('.is-link').click();
  cy.get('.notification-content').contains('Ranking');
});

// Test changing choice
it('Comparing Absolute #5', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(2)').click();
  cy.get('.is-link').click();
  cy.get('.notification-content').contains('Ranking');
});

// Test API post/get requests
it('Comparing Absolute - check post data sent to API', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.contains('Compare Absolute').click();
  cy.get(':nth-child(1) > .buttonContainer > :nth-child(3)').click();
  cy.get(':nth-child(2) > .buttonContainer > :nth-child(2)').click();
  cy.get(':nth-child(3) > .buttonContainer > :nth-child(1)').click();
  cy.get(':nth-child(4) > .buttonContainer > :nth-child(3)').click();
  cy.intercept({
    method: 'POST',
    url: '/props/*'
  }).as('postCheck');
  cy.get('.is-link').click();
  cy.wait('@postCheck').then(interception => {
    assert.isNotNull(interception.response.body, 'API post call has data');
  });
});

it('Comparing Absolute - check get data received from  API', () => {
  cy.visit('/');
  cy.get('.all-projects > .button').click();
  cy.get('.search-block > .control > .input').type('cypress{enter}');
  cy.contains('cypress').click();
  cy.intercept({
    method: 'GET',
    url: '/api/*'
  }).as('getCheck');
  cy.contains('Compare Absolute').click();
  cy.wait('@getCheck').then(interception => {
    assert.isNotNull(interception.response.body, 'API post call has data');
  });
});

it('Test logout', () => {
  cy.visit('/');
  cy.get('.navbar-burger').click();
  cy.contains('Logout').click();
});

it('Test without login', () => {
  cy.visit('#/project/119698/compare-absolute');
  // Check that we are going to login page
  cy.get(':nth-child(1) > .control > .input');
});
